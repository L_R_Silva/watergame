using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
/// <summary>
/// Abstract Class used as base of all Tiles Propertys and Methods
/// </summary>
[RequireComponent(typeof(MeshFilter)), RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(BoxCollider))]
public abstract class TileGeneric : MonoBehaviour
{
    /// <summary>
    ///This are the Types of Tiles that Exist. 
    ///The tile is Defaulted as Undefined on the world Creation.
    ///All tiles Must have a Type.
    ///Building just happens in Undefined Tiles. 
    /// </summary>
    public enum type
    {
        /// <summary>
        /// All Tiles Start as Undefined
        /// </summary>
        Undefined, //0
        ResidentialZone, //1
        IndustryZone, //2
        ComercialZone, //3
        CommunityZone, //4 
        AgriculturalZone, //5
        River //6
    };
    /// <summary>
    /// This are the possible States of a given Tile.
    /// The tile is always set by default as healthy.
    /// All tiles must have a state.
    /// When the pollution in a given tile is  high(#Define a value) change states to mid and unhealthy
    /// </summary>
    public enum state
    {
        /// <summary>
        /// Until 35 units of pollution
        /// </summary>
        Healthy,
        /// <summary>
        /// with more than 85 units of pollution
        /// </summary> 
        Unhealthy,
        /// <summary>
        /// With 36 units of pollution and Up
        /// </summary> 
        Midhealthy
    };
    /// <summary>
    /// Contains the neighborhood of a given tile 
    /// </summary>
    private List<TileGeneric> neighborhood;
    protected state stateTile; 
    protected type zoneType;
    protected int maxNumberOfPeople;
    protected float polutionGenerated;
    protected bool isBuildable; 
    /* 
        Residencial: 108,4 litros por dia,(Média nacional) 58,7% da agua e 58,8% dos esgostos.
        Agricola: 91,5 litros por Real produzido.
        Comercial: 50 a 80 litros por Ocupante Efetivo.
        Industrial: 3,72 litros por Real Produzido.
        AguaTratada: 33,0 m³/s
        TratamentoDeEsgotos: 11 m³/s
    */
    protected float waterUsed;
    protected float revenue;
    protected float constructionPrice;
    private void OnMouseDown()
    {
        OnClickShow();
    }    
    private void OnMouseEnter()
    {
        OnRoverShow();
    }
    private void OnMouseExit()
    {
        
    }
    /// <summary>
    ///ZoneTypeDefine() Define the atributes of the tiles based on Type.
    ///</summary>
    protected abstract void ZoneTypeDefine();
    ///<summary>
    ///OnClickShow() Shows a menu of options of what to build in a given type of Tile
    ///</summary>
    protected abstract void OnClickShow();
    ///<summary>
    ///OnRoverShow() Shows the current stats of a given Tile
    ///</summary>
    protected abstract void OnRoverShow();
    /// <summary>
    /// GetZoneType() Gets the Value of zoneType variable
    /// </summary>
    /// <returns>Returns string zoneType</returns>
    public type GetZoneType()
    {return zoneType;}
    /// <summary>
    /// GetMaxNumberOfPeople() Gets the value of maxNumberOfPeople variable
    /// </summary>
    /// <returns>Returns int maxNumberOfPeople</returns>
    public int GetMaxNumberOfPeople()
    {return maxNumberOfPeople;}
    /// <summary>
    /// GetPolutionGenerated() Gets the value of polutionGenerated variable
    /// </summary>
    /// <returns>Returns float polutionGenerated</returns>
    public float GetPolutionGenerated()
    {return polutionGenerated;}
    /// <summary>
    /// GetIsBuildable() Gets the value of isBuildable variable
    /// </summary>
    /// <returns>Returns bool isBuildable</returns>
    public bool GetIsBuildable()
    {return isBuildable;}
    /// <summary>
    /// GetWaterUsed() Gets the value of waterUsed variable
    /// </summary>
    /// <returns>Returns float waterUsed</returns>
    public float GetWaterUsed()
    {return waterUsed;}
    /// <summary>
    /// GetRevenue() Gets the value of revenue variable
    /// </summary>
    /// <returns>Returns float revenue</returns>
    public float GetRevenue()
    {return revenue;}
    /// <summary>
    /// GetconstructionPrice() Gets the value of constructionPrice variable
    /// </summary>
    /// <returns>Returns float constructionPrice</returns>
    public float GetConstructionPrice()
    {return constructionPrice;}
    /// <summary>
    /// GetStateTile() Gets the value of stateTile variable
    /// </summary>
    /// <returns>Retuns string stateTile</returns>
    public state GetStateTile()
    {return stateTile;}
    /// <summary>
    /// GetNeighborhood() Returns a list of the neighbors of a given cell
    /// </summary>
    /// <returns>Returns List of TileGeneric  neighborhood</returns>
    public List<TileGeneric> GetNeighborhood()
    {
        List<TileGeneric> _neighbors = new List<TileGeneric>();
        _neighbors = neighborhood;
        return _neighbors;
    }
    /// <summary>
    /// SetNeighborhood() Set the neighbors of a given Tile.
    /// </summary>
    /// <param name="neighbors"></param>
    public void SetNeighborhood(List<TileGeneric> neighbors)
    {
        neighborhood = new List<TileGeneric>();
        neighborhood = neighbors;
    }
    /// <summary>
    /// ChangeState() Changes the state of a given tile
    /// </summary>
    /// <param name="stateChangeTo"></param>
    public void ChangeState(state stateChangeTo)
    {stateTile = stateChangeTo;}
    
}