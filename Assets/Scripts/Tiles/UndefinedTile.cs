using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class UndefinedTile : TileGeneric 
{
    /// <summary>
    ///ZoneTypeDefine() Define the atributes of the tiles based on Type.
    ///</summary>
    protected override void ZoneTypeDefine()
    {
        zoneType = type.Undefined;
        stateTile = state.Healthy;
        maxNumberOfPeople = 0;
        polutionGenerated = 0f;
        isBuildable = true;
        waterUsed = 0f;
        revenue = 0f;
        constructionPrice = 0f;
    }
    protected override void OnClickShow()
    {
        BoardManagerGame.instance.ChangeTile( this.gameObject, BoardManagerGame.instance.tiles[3]);
    }
    protected override void OnRoverShow()
    {
        
    }
    private void Awake()
    {
        ZoneTypeDefine();
    }
   
}