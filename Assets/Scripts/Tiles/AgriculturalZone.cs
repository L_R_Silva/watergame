﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class AgriculturalZone : TileGeneric {

	/// <summary>
    ///ZoneTypeDefine() Define the atributes of the tiles based on Type.
    ///</summary>
	protected override void ZoneTypeDefine()
	{
		zoneType = type.AgriculturalZone;
        stateTile = state.Healthy;
        maxNumberOfPeople = (int)Mathf.Round(UnityEngine.Random.Range(20, 40));
        polutionGenerated = Mathf.Round(UnityEngine.Random.Range(95f,130f));
        isBuildable = true;
        waterUsed = 108.4f * maxNumberOfPeople;
        revenue = 0f;
        constructionPrice = 75f;
	}
    ///<summary>
    ///OnClickShow() Shows a menu of options of what to build in a given type of Tile
    ///</summary>
    protected override void OnClickShow()
    {

    }
    ///<summary>
    ///OnRoverShow() Shows the current stats of a given Tile
    ///</summary>
    protected override void OnRoverShow()
    {
        
    }
	private void Awake() {
		ZoneTypeDefine();
    }

}
