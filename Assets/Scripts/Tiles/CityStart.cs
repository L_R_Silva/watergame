﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CityStart : TileGeneric {

	/// <summary>
    ///ZoneTypeDefine() Define the atributes of the tiles based on Type.
    ///</summary>
	protected override void ZoneTypeDefine()
	{
		zoneType = type.CommunityZone;
        stateTile = state.Healthy;
        maxNumberOfPeople = (int)Mathf.Round(UnityEngine.Random.Range(20, 40));
        polutionGenerated = Mathf.Round(UnityEngine.Random.Range(95f,130f));
        isBuildable = false;
        waterUsed = 108.4f * maxNumberOfPeople;
        revenue = 0f;
        constructionPrice = 75f;
	}
    protected override void OnClickShow()
    {

    }
    protected override void OnRoverShow()
    {
        
    }
	private void Awake() {
		ZoneTypeDefine();
       
	}
}
