﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BoardManagerGame : MonoBehaviour {
	private static BoardManagerGame BoardManagerInstance = null;
	/// <summary>
	///    Undefined -> 0;
    ///    River -> 1;
    ///    CityStart -> 2;
    ///    ResidentialZone -> 3;
    ///    IndustryZone -> 4; 
    ///    CommunityZone -> 5;
    ///    CommercialZone -> 6;
	///    AgriculturalZone -> 7;
	/// </summary>
	[Header("Types of Tiles:")]
	public GameObject [] tiles;
	/// <summary>
	/// Static variable for use of public methods and variables from BoardManagerGame()
	/// </summary>
	public static BoardManagerGame instance {
		get{
			if(BoardManagerInstance == null)
			{
				BoardManagerInstance = FindObjectOfType(typeof (BoardManagerGame)) as BoardManagerGame;
			}

			if(BoardManagerInstance == null)
			{
				GameObject obj = new GameObject("BoardManager");
				BoardManagerInstance = obj.AddComponent(typeof(BoardManagerGame)) as BoardManagerGame;
			}	
			return BoardManagerInstance;
		}
	}
	/// <summary>
	/// ChangeTile() destroys an tile and replace it for the tile that the player choose
	/// </summary>
	/// <param name="ChangeFrom", name="ChangeTO"></param>
	public void ChangeTile(GameObject ChangeFrom, GameObject ChangeTO)
	{
		GameObject _ChangeTO = Instantiate(ChangeTO);
		_ChangeTO.gameObject.transform.position = ChangeFrom.gameObject.transform.position;
		if(_ChangeTO.GetComponent<TileGeneric>().GetZoneType() != TileGeneric.type.IndustryZone)
		{
			_ChangeTO.gameObject.transform.rotation = ChangeFrom.gameObject.transform.rotation;
		}
		_ChangeTO.gameObject.GetComponent<TileGeneric>().SetNeighborhood(ChangeFrom.gameObject.GetComponent<TileGeneric>().GetNeighborhood());
		DestroyImmediate(ChangeFrom);
		for(int x = 0; x <= GridGenerator.Cells.GetUpperBound(0); x++)
		{
			for(int y = 0; y <= GridGenerator.Cells.GetUpperBound(1); y++)
			{
				if(!GridGenerator.Cells[x,y])
				{
					GridGenerator.Cells[x,y] = _ChangeTO.GetComponent<TileGeneric>();
				}
			}
		}
	}
	/// <summary>
	/// ChangeState() Change the Health state of a Tile
	/// </summary>
	/// <param name="toChangeState"></param>
	public void ChangeState(GameObject toChangeState)
	{

	}
}	
	

