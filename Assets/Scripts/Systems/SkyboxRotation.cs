﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxRotation : MonoBehaviour 
{
	[SerializeField][Range(0f,10f)]private float rotationSpeed = 1f;
	void Update () {
		RenderSettings.skybox.SetFloat("_Rotation", Time.time * rotationSpeed);		
	}
}
