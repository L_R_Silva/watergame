﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Generates a Grid of tiles with UndefinedTiles, RiverZones and one StartPoint 
/// </summary>
public class GridGenerator : MonoBehaviour {
	/// <summary>
	/// Variable that represents the horizontal Size of the bidimensional array;
	/// </summary>
	[Header("Size of the Grid:")] 	
	[SerializeField]private int horizontalSize = 5;
	/// <summary>
	/// Variable that represents the vertical Size of the bidimensional array;
	/// </summary> 
	[SerializeField]private int verticalSize = 5;
	/// <summary>
	/// the bidimensional array Cells holds all the Cells in the grid and its a 5 by 5 by default
	/// </summary>
	public static TileGeneric [,] Cells;
	/// <summary>
	/// Array with only the RiverCells
	/// </summary>
	private TileGeneric [] RiverCells;
	/// <summary>
	/// Auxiliar variable to define which side of the grid the river will start
	/// </summary>
	private int side;
	/// <summary>
	/// The position on the Cells array which the river will start
	/// </summary>
	private int startRiverPosition;
	private void Awake() 
	{
		InitGrid();
 	}
	/// <summary>
	/// InitGrid() Initialize Grid with a River, one citystart and undefined tiles;
	/// </summary>
	private void InitGrid()
	{
		InstantiateGridPieces();
		SetNeighborhood();
		SetRiverTiles();
		SetCityStartTile();
		//for(int x = 0; x < horizontalSize; x++)
		//	for(int y = 0; y < verticalSize; y++)
		//	{
		//		Debug.Log(Cells[x,y].GetZoneType() + " {" + x +","+y+"}");
		//	}
		//Debug.Log(GridGenerator.Cells.GetUpperBound(1));
		
	}
	/// <summary>
	/// InstantiateGridPieces() Instantiate the pieces of the grid based of the Prefabs
	/// </summary>
	private void InstantiateGridPieces()
	{	
		Cells = new TileGeneric[horizontalSize, verticalSize];
		float []_rot = {0f,90f,180f};
		for(int x = 0; x < horizontalSize; x++)
		{
			GameObject _obj = Instantiate(BoardManagerGame.instance.tiles[0]);
			if(x > 0)
			{
				Cells[x,0] = _obj.GetComponent<TileGeneric>();
				Cells[x,0].gameObject.SetActive(true);
				Cells[x,0].gameObject.transform.position = new Vector3(Cells[x-1,0].gameObject.transform.position.x + 10f,0f,0f);
				Cells[x,0].gameObject.transform.rotation = Quaternion.Euler(0f,_rot[(int)Mathf.Round(UnityEngine.Random.Range(0, 3))],0f);
			}
			for(int y = 0; y < verticalSize; y++)
			{
				if(x == 0 && y == 0)
				{
					Cells[x,y] = _obj.GetComponent<TileGeneric>();
					Cells[x,y].gameObject.SetActive(true);
				}
				if(y > 0)
				{
					_obj = Instantiate(BoardManagerGame.instance.tiles[0]);
					Cells[x,y] = _obj.GetComponent<TileGeneric>();
					Cells[x,y].gameObject.SetActive(true);
					Cells[x,y].gameObject.transform.position = new Vector3(Cells[x,0].gameObject.transform.position.x, 0f, Cells[x,y-1].gameObject.transform.position.z - 10f);
					Cells[x,y].gameObject.transform.rotation = Quaternion.Euler(0f,_rot[(int)Mathf.Round(UnityEngine.Random.Range(0, 3))],0f);
				}
			}	
		}
	}
	/// <summary>
	/// GetNeighborhood() Gets the moore neighborhood of a given tile of size 1
	/// </summary>
	private void SetNeighborhood()
	{
		for(int x = 0; x < horizontalSize; x++)
		{
			for(int y = 0; y < verticalSize; y++)
			{
				List<TileGeneric> _neighbors = new List<TileGeneric>();
				//Top Cell
				if(y > 0)
				{
					_neighbors.Add(Cells[x,y-1]);
				}
				//Down Cell
				if(y < verticalSize-1)
				{
					_neighbors.Add(Cells[x,y+1]);
				}
				//Left Cell
				if(x > 0)
				{
					_neighbors.Add(Cells[x-1,y]);
				}
				//Right Cell
				if(x < horizontalSize-1)
				{
					_neighbors.Add(Cells[x+1,y]);
				}
				//Left Top Cell
				if(x > 0 && y > 0)
				{
					_neighbors.Add(Cells[x-1,y-1]);
				}
				//Left Down Cell
				if(x > 0 && y < verticalSize-1)
				{
					_neighbors.Add(Cells[x-1,y+1]);
				}
				//Right Top Cell
				if(x < horizontalSize - 1 && y > 0)
				{
					_neighbors.Add(Cells[x+1,y-1]);
				}
				//Right Down Cell
				if(x < horizontalSize - 1 && y < verticalSize-1)
				{
					_neighbors.Add(Cells[x+1,y+1]);
				}
				if(_neighbors != null)
				{
					Cells[x,y].SetNeighborhood(_neighbors);
				}
			}
			
		}

	}
	/// <summary>
	/// SetRiverTiles() Sets Where the in which Axis the river you cut the grid (In this version the River is always a straight line)
	/// </summary>
	private void SetRiverTiles()
	{
		side = (int)Mathf.Round(UnityEngine.Random.Range(0,2));
		float []_rot = {-90f,90f};
		if(side == 0)
		{
			startRiverPosition = (int)Mathf.Round(UnityEngine.Random.Range(1,horizontalSize-1));
			RiverCells = new TileGeneric[horizontalSize];
			for(int y = 0; y < verticalSize; y++)
			{
				if(Cells[startRiverPosition,y].GetNeighborhood()[2] != null)
				{
					GameObject _riverPath = Instantiate(BoardManagerGame.instance.tiles[1]);
					_riverPath.transform.position = Cells[startRiverPosition,y].transform.position;
					_riverPath.transform.rotation = Quaternion.Euler(0f,_rot[(int)Mathf.Round(UnityEngine.Random.Range(0, 2))],0f);
					_riverPath.GetComponent<TileGeneric>().SetNeighborhood(Cells[startRiverPosition,y].GetNeighborhood());
					Destroy(Cells[startRiverPosition,y].gameObject);
					Cells[startRiverPosition,y] = _riverPath.GetComponent<TileGeneric>();
					RiverCells[y] = _riverPath.GetComponent<TileGeneric>();
				}
			}
		}
		else
		{
			startRiverPosition = (int)Mathf.Round(UnityEngine.Random.Range(1,verticalSize-1));
			RiverCells = new TileGeneric[verticalSize];
			for(int x = 0; x < horizontalSize; x++)
			{
				if(Cells[x,startRiverPosition].GetNeighborhood()[4] != null)
				{
					GameObject _riverPath = Instantiate(BoardManagerGame.instance.tiles[1]);
					_riverPath.transform.position = Cells[x,startRiverPosition].transform.position;
					_riverPath.transform.rotation = Quaternion.Euler(0f,0f,0f);
					_riverPath.GetComponent<TileGeneric>().SetNeighborhood(Cells[x,startRiverPosition].GetNeighborhood());
					Destroy(Cells[x,startRiverPosition].gameObject);
					Cells[x,startRiverPosition] = _riverPath.GetComponent<TileGeneric>();
					RiverCells[x] = _riverPath.GetComponent<TileGeneric>();
				}

			}
			//GameObject riverStart = Cells[0,(int)Mathf.Round(UnityEngine.Random.Range(2,verticalSize-1))];
		}
		 
	}
	/// <summary>
	/// SetCityStartTile() Sets Where the first tile of the city will be placed (In this version the CityTile will always be in the neighboorhood of size 1 of a given Tile)
	/// </summary>
	private void SetCityStartTile()
	{
		if(side > 0)
		{
			int _randomIndex = (int)Mathf.Round(UnityEngine.Random.Range(1,horizontalSize-1));
			int _sideOftheRiver = (int)Mathf.Round(UnityEngine.Random.Range(0,2));
			if(_sideOftheRiver < 1)
			{
				GameObject _startCity = Instantiate(BoardManagerGame.instance.tiles[2]);
				_startCity.transform.position = RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].transform.position;
				_startCity.GetComponent<TileGeneric>().SetNeighborhood(RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].GetNeighborhood());
				Destroy(RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].gameObject);
				Cells[_randomIndex, startRiverPosition-1] = _startCity.GetComponent<TileGeneric>();
				//int _indexY = startRiverPosition-1;
				//Debug.Log(Cells[_randomIndex, startRiverPosition-1].GetZoneType() +" TOP" + " {" + _randomIndex + "," + _indexY  + "}");
			}
			else
			{
				GameObject _startCity = Instantiate(BoardManagerGame.instance.tiles[2]);
				_startCity.transform.position = RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].transform.position;
				_startCity.GetComponent<TileGeneric>().SetNeighborhood(RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].GetNeighborhood());
				Destroy(RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].gameObject);
				Cells[_randomIndex, startRiverPosition+1] = _startCity.GetComponent<TileGeneric>();
				//int _indexY = startRiverPosition+1;
				//Debug.Log(Cells[_randomIndex, startRiverPosition+1].GetZoneType()+ " DOWN" + " {" + _randomIndex + "," + _indexY + "}");
			}
			
		}
		else
		{
			int _randomIndex = (int)Mathf.Round(UnityEngine.Random.Range(1,verticalSize-1));
			int _sideOftheRiver = (int)Mathf.Round(UnityEngine.Random.Range(3,5));
			if(_sideOftheRiver < 4)
			{
				GameObject _startCity = Instantiate(BoardManagerGame.instance.tiles[2]);
				_startCity.transform.position = RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].transform.position;
				_startCity.GetComponent<TileGeneric>().SetNeighborhood(RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].GetNeighborhood());
				Destroy(RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].gameObject);
				Cells[startRiverPosition+1, _randomIndex] = _startCity.GetComponent<TileGeneric>();
				//int _indexX = startRiverPosition+1;
				//Debug.Log(Cells[startRiverPosition+1, _randomIndex].GetZoneType() + " RIGHT" + " {" +_indexX + "," + _randomIndex + "}");
			}
			else
			{
				GameObject _startCity = Instantiate(BoardManagerGame.instance.tiles[2]);
				_startCity.transform.position = RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].transform.position;
				_startCity.GetComponent<TileGeneric>().SetNeighborhood(RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].GetNeighborhood());
				Destroy(RiverCells[_randomIndex].GetNeighborhood()[_sideOftheRiver].gameObject);
				Cells[startRiverPosition-1, _randomIndex] = _startCity.GetComponent<TileGeneric>();
				//int _indexX = startRiverPosition -1;
				//Debug.Log(Cells[startRiverPosition-1, _randomIndex].GetZoneType() + " LEFT" + " {" +_indexX + "," + _randomIndex + "}");
			}
		}
	}
}